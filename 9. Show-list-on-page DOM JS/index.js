// const arr = ['Kharkiv', 'Kiev', 'Odessa', 'Lviv', 'Dnieper'];

// const getArr = (arr, parent = document.body) => {
//     const list = document.createElement('ul');
//     parent.prepend(list);

//     arr.forEach(element => {
//         const item = document.createElement('li');
//         item.innerHTML = element;
//         list.prepend(item);
//     });
// };

// getArr(arr);

const arr2 = ['Kharkiv', 'Kiev', ['Kovel', ['Kiev'], 'Lutsk'], 'Odessa', 'Lviv', 'Dnieper'];

const getArr2 = (arr, parent = document.body) => {
    const list = document.createElement('ul');
    arr.forEach(element => {
        const item = document.createElement('li');
        if (Array.isArray(element)) {
            getArr2(element, item);
        } else {
            item.innerHTML = element;
        }
        list.prepend(item);
    });
    parent.prepend(list);
};

getArr2(arr2);
